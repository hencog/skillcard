[
    {
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Quote",
        "alias": "quote",
        "view": "textstring",
        "icon": "icon-quote",
        "config": {
            "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-variant: italic; font-size: 18px",
            "markup": "<blockquote>#value#</blockquote>"
        }
    },
    {
        "name": "Infobox",
        "alias": "infobox",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-info",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Background Image",
                    "alias": "backgroundImage",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59",
                    "description": "Image to be displayed at right of infobox."
                },
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Title of the infobox."
                },
                {
                    "name": "Description",
                    "alias": "description",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Description for the infobox."
                },
                {
                    "name": "Link",
                    "alias": "link",
                    "propretyType": {},
                    "dataType": "a6857c73-d6e9-480c-b6e6-f15f6ad11125",
                    "description": "The page you want to link to."
                },
                {
                    "name": "Position",
                    "alias": "position",
                    "propretyType": {},
                    "dataType": "a1f50886-00cd-435f-9ef3-28321d0d1829",
                    "description": "",
                    "prevalues": [
                        "Left",
                        "Middle",
                        "Right"
                    ]
                }
            ],
            "expiration": 0,
            "frontView": "",
            "renderInGrid": "0"
        }
    },
    {
        "name": "Home Link",
        "alias": "homeLink",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-link",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Icon",
                    "alias": "icon",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59",
                    "description": "Icon to be displayed"
                },
                {
                    "name": "Title",
                    "alias": "title",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Description",
                    "alias": "description",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Link",
                    "alias": "link",
                    "propretyType": {},
                    "dataType": "a6857c73-d6e9-480c-b6e6-f15f6ad11125",
                    "description": "Page you want to link to"
                }
            ],
            "frontView": ""
        }
    },
    {
        "name": "Download Block",
        "alias": "downloadBlock",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "image",
                    "alias": "image",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Block Title",
                    "alias": "blockTitle",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Download 1",
                    "alias": "download1",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 1 Label",
                    "alias": "download1Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                },
                {
                    "name": "Download 2",
                    "alias": "download2",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 2 Label",
                    "alias": "download2Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                },
                {
                    "name": "Download 3",
                    "alias": "download3",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 3 Label",
                    "alias": "download3Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                },
                {
                    "name": "Download 4",
                    "alias": "download4",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 4 Label",
                    "alias": "download4Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                },
                {
                    "name": "Download 5",
                    "alias": "download5",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 5 Label",
                    "alias": "download5Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                },
                {
                    "name": "Download 6",
                    "alias": "download6",
                    "propretyType": {},
                    "dataType": "93929b9a-93a2-4e2a-b239-d99334440a59"
                },
                {
                    "name": "Download 6 Label",
                    "alias": "download6Label",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": "Label for the file."
                }
            ],
            "frontView": "",
            "expiration": 1
        }
    }
]